**Description**

The project builds two small executables: *sscc* and *unittest*.

*sscc* first asks for the size of a sscc's company prefix (or id), then a sscc source is selected (either read from a file or manually entered from command line). If file reading is selected, a path to the file, containing a list of sscc scans is entered. A sample *scans.txt* file is provided. If manual entering is selected, the user is prompted to enter a sccc. The validity of each read or manually entered sscc is checked and the result is shown on the command line. 

*unittest* simply executes a set of unit tests that cover all possible results of an sscc validity checking. A success message is shown at command line if all unit tests pass, otherwise a failed assertion is shown.


**Source code**

- *sscc.hpp* and *sscc.cpp* implement a class that parses and holds a valid sscc. 
- *main.cpp* mainly contains user interaction.
- *unittest.cpp* defines a set of unit test regarding sscc parsing and asserts their validity.


**Build**

```
 mkdir build
 cd build
 cmake ..
 make
```

**Run**


Reading scanes from a file:
```
./sscc
 Enter the lenght of a company prefix:
 7
 Read scans from a file or enter them manually? (r/m)
 r
 Enter the file name:
 ../scanes.txt
 Check 00342603111307076543      : OK, (00) 3426031 1130707654 3
 Check 02442603111307076526      : Invalid application identifier!
 Check 0034260311130707619       : Invalid size!
 Check 00342603111307076504      : Invalid check digit!
 Check 00342603111307076499      : OK, (00) 3426031 1130707649 9
 Check 00342603211307074624      : Invalid check digit!

 ...
```
Manual entering of scanes:
```
./sscc
 Enter the lenght of a company prefix:
 7
 Read scans from a file or enter them manually? (r/m)
 m
 Enter a scan:
 00342603111307076543
 OK, (00) 3426031 1130707654 3

 Enter a scan:
 00342603111307076504
 Invalid check digit!

 ...
```
Unit test:
```
./unittest
 All tests passed.
 
```



