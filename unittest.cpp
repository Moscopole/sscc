#include <iostream>
#include <cassert>

#include "sscc.hpp"

std::string_view testParse(const std::string_view& test, size_t size)
{
    std::string_view status;
    SSCC::Parse(test, size, status);
    return status;
}

int main(int argc, char** argv) 
{
    assert(testParse("00342603111307076543", 7) == "OK");
    assert(testParse("02442603111307076526", 7) == "Invalid application identifier!");
    assert(testParse("0034260311130707619", 7) == "Invalid size!");
    assert(testParse("00342603111307076504", 7) == "Invalid check digit!");
    assert(testParse("00342603111307076499", 7) == "OK");
    assert(testParse("00342603211307074624", 7) == "Invalid check digit!");

    assert(testParse("00342603111307076543", 6) == "OK");
    assert(testParse("00342603111307076543", 5) == "Invalid data size!");
    assert(testParse("00342603111307076543", 10) == "Invalid data size!");
    assert(testParse("00342603111307076543", 9) == "OK");

    assert(testParse("003426031113O7076543", 7) == "SSCC contains non-digit characters!");

    std::cout << "All tests passed." << std::endl;
    return 0;
}