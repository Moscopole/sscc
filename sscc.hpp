#pragma once

#include <memory>
#include <string_view>

class SSCC
{
public:
    static std::unique_ptr<SSCC> Parse(const std::string_view& scan, size_t companyPrefixSize, std::string_view& status);
    friend std::ostream& operator<<(std::ostream& os, const SSCC& sscc);

private:
    constexpr static size_t size_ {20}; 
    constexpr static std::string_view applicationIdentifier_{"00"}; 
    
    std::string_view companyPrefix_;
    std::string_view serialNumber_;

    const unsigned char CheckDigit_() const;
};

