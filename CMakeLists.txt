cmake_minimum_required(VERSION 3.10)

project(sscc)

set(CMAKE_CXX_STANDARD 17)

add_executable(sscc main.cpp sscc.cpp)
add_executable(unittest unittest.cpp sscc.cpp)
