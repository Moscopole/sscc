#include <iostream>
#include <fstream>

#include "sscc.hpp"

int main(int argc, char** argv) 
{
    size_t size;
    std::cout << "Enter the lenght of a company prefix:\n";
    std::cin >> size;

    std::string scan;
    std::string_view status;

    unsigned char action;
    std::cout << "Read scans from a file or enter them manually? (r/m)\n";
    do
    {
        std::cin >> action;
    }
    while(action != 'm' && action != 'r');

    while(1)
    {
        if(action == 'r')
        {
            std::string fileName;
            std::cout << "Enter the file name:\n";
            std::cin >> fileName;

            std::ifstream savedScanes;
            savedScanes.open(fileName);
            if (savedScanes.is_open())
            {
                while(savedScanes >> scan)
                {
                    std::cout << "Check " << scan << "\t: ";
                    auto sscc = SSCC::Parse(scan, size, status);

                    std::cout << status; 
                    if(sscc != 0) std::cout << ", " << *sscc.get();
                    std::cout << "\n";
                }
                std::cout << "\n";
                savedScanes.close();
            }
        }
        else
        {
                std::cout << "Enter a scan:\n";
                std::cin >> scan;

                auto sscc = SSCC::Parse(scan, size, status);

                std::cout << status; 
                if(sscc != 0) std::cout << ", " << *sscc.get();
                std::cout << "\n\n";
        }
    }
    
    return 0;
}
