#include <algorithm>
#include <iomanip>

#include "sscc.hpp"

std::unique_ptr<SSCC> SSCC::Parse(const std::string_view& scan, size_t companyPrefixSize, std::string_view& status)
{
    //should check and remove '(', ')', FNC, ...

    if(scan.size() != SSCC::size_)
    {
        status = "Invalid size!";
        return 0;
    }

    if(std::find_if(scan.begin(), scan.end(), 
                    [](auto c) { return !std::isdigit(c); }) != scan.end())
    {
        status = "SSCC contains non-digit characters!";
        return 0;
    }

    if(std::string_view{&scan.at(0), 2} != SSCC::applicationIdentifier_)
    {
        status = "Invalid application identifier!";
        return 0;
    }

    auto companyPrefixBegin{scan.at(2) == '0' ? 3:2};
    auto serialNumberEnd{scan.at(2) == '0' ? 16:17};

    if(companyPrefixSize < 6 || companyPrefixSize > 9 ) 
    {
        status = "Invalid data size!";
        return 0;
    }

    std::unique_ptr<SSCC> sscc = std::make_unique<SSCC>();
    sscc->companyPrefix_ = std::string_view{&scan.at(companyPrefixBegin), companyPrefixSize};        
    sscc->serialNumber_ = std::string_view{&scan.at(companyPrefixBegin+companyPrefixSize), serialNumberEnd-companyPrefixSize}; 
    if(sscc->CheckDigit_() != scan.back())
    {
        status = "Invalid check digit!";
        return 0;
    }

    status = "OK";
    return sscc;       
} 

const unsigned char SSCC::CheckDigit_() const
{
    size_t i = 0;
    int sum = 0;
    for(auto part : {serialNumber_, companyPrefix_})
        for (part; !part.empty(); part.remove_suffix(1))
            sum+= (part.back()-'0') * (i++%2 == 0? 3:1);

    return (10 - sum%10) + '0';
}

std::ostream& operator<<(std::ostream& os, const SSCC& sscc)
{
    os << "(" << sscc.applicationIdentifier_ << ") " << sscc.companyPrefix_ << " "
        << sscc.serialNumber_ << " " << sscc.CheckDigit_();
    return os;
}
